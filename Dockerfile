FROM maven:3.6-adoptopenjdk-14

WORKDIR /app
ADD . /app

RUN mvn clean install

CMD ["mvn","spring-boot:run"]
