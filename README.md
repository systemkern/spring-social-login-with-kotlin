[![Source Repo](https://img.shields.io/badge/fork%20on-gitlab-important?logo=gitlab)](https://gitlab.com/systemkern/spring-social-login-with-kotlin)
[![Gitlab Pipelines](https://gitlab.com/systemkern/works-on-my-machine/badges/master/pipeline.svg)](https://gitlab.com/systemkern/spring-social-login-with-kotlin/-/pipelines)
[![Twitter @systemkern](https://img.shields.io/badge/follow-%40systemkern-blue?logo=twitter)](https://twitter.com/systemkern)
[![LinkedIn @systemkern](https://img.shields.io/badge/contact%20me-%40systemkern-blue?logo=linkedin)](https://linkedin.com/in/systemkern)



Spring Social Login with Kotlin
===============================

Getting Started
--------------------

```
docker-compose up --build
```
