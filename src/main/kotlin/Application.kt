package com.systemkern.springsocialkotlin

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.security.core.AuthenticationException
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@SpringBootApplication
@ConfigurationPropertiesScan
class SpringSocialApplication

fun main() {
    SpringApplication.run(SpringSocialApplication::class.java)
}

@ConfigurationProperties(prefix = "app")
class Config {
    lateinit var tokenSecret: String
    var tokenExpirationMsec: Long = 480_000
    lateinit var oAuth2AuthorizedRedirectUris: List<String>
}


@Configuration
class WebMvcConfig : WebMvcConfigurer {
    private val MAX_AGE_SECS: Long = 3600
    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
            .allowedOrigins("*")
            .allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS")
            .allowedHeaders("*")
            .allowCredentials(true)
            .maxAge(MAX_AGE_SECS)
    }
}


@ResponseStatus(BAD_REQUEST)
class BadRequestException : RuntimeException {
    constructor(message: String?) : super(message)
    constructor(message: String?, cause: Throwable?) : super(message, cause)
}

class OAuth2AuthenticationProcessingException : AuthenticationException {
    constructor(msg: String?, t: Throwable?) : super(msg, t)
    constructor(msg: String?) : super(msg)
}

@ResponseStatus(HttpStatus.NOT_FOUND)
class ResourceNotFoundException(
    val resourceName: String?,
    val fieldName: String?,
    val fieldValue: Any?,
) : RuntimeException(String.format("%s not found with %s : '%s'", resourceName, fieldName, fieldValue))
