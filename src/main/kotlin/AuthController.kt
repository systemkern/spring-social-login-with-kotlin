package com.systemkern.springsocialkotlin

import com.systemkern.springsocialkotlin.security.createToken
import javax.validation.Valid
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder

@RestController
@RequestMapping("/auth")
internal class AuthController(
    private val userRepository: UserRepository,
    private val authenticationManager: AuthenticationManager,
    private val passwordEncoder: PasswordEncoder,
    private val config: Config,
) {
    @PostMapping("/login")
    fun authenticateUser(@Valid @RequestBody loginRequest: LoginRequest): ResponseEntity<*> {
        val authentication = authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(
                loginRequest.email,
                loginRequest.password
            )
        )
        SecurityContextHolder.getContext().authentication = authentication
        val token = createToken(authentication = authentication, secret = config.tokenSecret, expiration = config.tokenExpirationMsec)
        return ResponseEntity.ok(AuthResponse(accessToken = token))
    }

    @PostMapping("/signup")
    fun registerUser(@Valid @RequestBody signUpRequest: SignUpRequest): ResponseEntity<*> {
        if (userRepository.existsByEmail(signUpRequest.email))
            throw BadRequestException("Email address already in use.")

        // Creating user's account
        val user = userRepository.save(User(
            name = signUpRequest.name,
            email = signUpRequest.email,
            password = passwordEncoder.encode(signUpRequest.password)!!,
            provider = AuthProvider.local,
        ))
        val location = ServletUriComponentsBuilder
            .fromCurrentContextPath().path("/user/me")
            .buildAndExpand(user.id).toUri()
        return ResponseEntity.created(location)
            .body(ApiResponse(message = "User registered successfully"))
    }
}

private class ApiResponse(val isSuccess: Boolean = true, val message: String = "")

private class AuthResponse(
    val accessToken: String,
    val tokenType: String = "Bearer",
)

class LoginRequest(
    @NotBlank @Email val email: String,
    @NotBlank val password: String,
)

class SignUpRequest(
    @NotBlank @Email val email: String,
    @NotBlank val name: String,
    @NotBlank val password: String,
)
