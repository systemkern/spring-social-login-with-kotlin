package com.systemkern.springsocialkotlin.security

import com.nimbusds.oauth2.sdk.util.StringUtils
import java.util.Base64
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest
import org.springframework.stereotype.Component
import org.springframework.util.SerializationUtils

const val OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME = "oauth2_auth_request"
const val REDIRECT_URI_PARAM_COOKIE_NAME = "redirect_uri"
private const val cookieExpireSeconds = 180


@Component
class HttpCookieOAuth2AuthorizationRequestRepository : AuthorizationRequestRepository<OAuth2AuthorizationRequest?> {
    override fun loadAuthorizationRequest(request: HttpServletRequest): OAuth2AuthorizationRequest? =
        request.cookies?.find { it.name == OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME }
            ?.value
            ?.let { SerializationUtils.deserialize(Base64.getUrlDecoder().decode(it)) }
            as OAuth2AuthorizationRequest?

    override fun saveAuthorizationRequest(authorizationRequest: OAuth2AuthorizationRequest?, request: HttpServletRequest, response: HttpServletResponse) {
        if (authorizationRequest == null) {
            request.deleteCookie(response, OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME)
            request.deleteCookie(response, REDIRECT_URI_PARAM_COOKIE_NAME)
            return
        }

        response.addCookie(
            name = OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME,
            value = Base64.getUrlEncoder().encodeToString(SerializationUtils.serialize(authorizationRequest)),
            maxAge = cookieExpireSeconds,
        )
        val redirectUriAfterLogin = request.getParameter(REDIRECT_URI_PARAM_COOKIE_NAME)
        if (StringUtils.isNotBlank(redirectUriAfterLogin))
            response.addCookie(REDIRECT_URI_PARAM_COOKIE_NAME, redirectUriAfterLogin, cookieExpireSeconds)
    }

    override fun removeAuthorizationRequest(request: HttpServletRequest): OAuth2AuthorizationRequest? =
        loadAuthorizationRequest(request)

    fun removeAuthorizationRequestCookies(request: HttpServletRequest, response: HttpServletResponse) {
        request.deleteCookie(response, OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME)
        request.deleteCookie(response, REDIRECT_URI_PARAM_COOKIE_NAME)
    }

    private fun HttpServletRequest.deleteCookie(response: HttpServletResponse, name: String) {
        cookies?.find { it.name == name }?.apply {
            this.value = ""
            this.path = "/"
            this.maxAge = 0
            response.addCookie(this)
        }
    }
}


fun HttpServletResponse.addCookie(name: String, value: String, maxAge: Int) {
    val cookie = Cookie(name, value)
    cookie.path = "/"
    cookie.isHttpOnly = true
    cookie.maxAge = maxAge
    this.addCookie(cookie)
}

