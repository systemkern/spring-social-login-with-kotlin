package com.systemkern.springsocialkotlin.security

import com.systemkern.springsocialkotlin.AuthProvider
import com.systemkern.springsocialkotlin.OAuth2AuthenticationProcessingException
import com.systemkern.springsocialkotlin.User
import com.systemkern.springsocialkotlin.UserRepository
import org.springframework.security.authentication.InternalAuthenticationServiceException
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest
import org.springframework.security.oauth2.core.user.OAuth2User
import org.springframework.stereotype.Service

@Service
class OAuth2UserService(
    private val userRepository: UserRepository,
) : DefaultOAuth2UserService() {

    override fun loadUser(oAuth2UserRequest: OAuth2UserRequest): OAuth2User {
        val oAuth2User = super.loadUser(oAuth2UserRequest)
        return try {
            val oAuth2UserInfo = when {
                oAuth2UserRequest.clientRegistration.registrationId.equals(AuthProvider.google.toString(), ignoreCase = true) -> GoogleOAuth2UserInfo(oAuth2User.attributes)
                oAuth2UserRequest.clientRegistration.registrationId.equals(AuthProvider.facebook.toString(), ignoreCase = true) -> FacebookOAuth2UserInfo(oAuth2User.attributes)
                oAuth2UserRequest.clientRegistration.registrationId.equals(AuthProvider.github.toString(), ignoreCase = true) -> GithubOAuth2UserInfo(oAuth2User.attributes)
                else -> throw OAuth2AuthenticationProcessingException("Sorry! Login with ${oAuth2UserRequest.clientRegistration.registrationId} is not supported yet.")
            }
            if (oAuth2UserInfo.email.isBlank())
                throw OAuth2AuthenticationProcessingException("Email not found from OAuth2 provider")
            val dbUser = (userRepository.findByEmail(oAuth2UserInfo.email)
                ?.let {user ->
                    if (user.provider != AuthProvider.valueOf(oAuth2UserRequest.clientRegistration.registrationId))
                        throw OAuth2AuthenticationProcessingException("Looks like you're signed up with a ${user.provider} account. Please use your ${user.provider} account to login.")
                    // update and save existing user
                    user.copy(
                        name = oAuth2UserInfo.name,
                        imageUrl = oAuth2UserInfo.imageUrl,
                    )
                }
                ?: User(
                    name = oAuth2UserInfo.name,
                    email = oAuth2UserInfo.email,
                    imageUrl = oAuth2UserInfo.imageUrl,
                    provider = AuthProvider.valueOf(oAuth2UserRequest.clientRegistration.registrationId),
                    providerId = oAuth2UserInfo.id,
                ))
                .let { userRepository.save(it) }

            UserAuthPrincipal(
                id = dbUser.id,
                email = dbUser.email,
                password = dbUser.password,
                attributes = oAuth2User.attributes,
            )
        } catch (ex: Exception) {
            // Throwing an instance of AuthenticationException will trigger the OAuth2AuthenticationFailureHandler
            throw InternalAuthenticationServiceException(ex.message, ex.cause)
        }
    }
}


private abstract class OAuth2UserInfo(var attributes: Map<String, Any>) {
    abstract val id: String
    abstract val name: String
    abstract val email: String
    abstract val imageUrl: String?
}

private class FacebookOAuth2UserInfo(attributes: Map<String, Any>) : OAuth2UserInfo(attributes) {
    // Kotlin's 'by Map' feature is the equivalent of super.attributes["id"]
    override val id: String by super.attributes
    override val name: String by super.attributes
    override val email: String by super.attributes
    override val imageUrl: String?
        get() = ((attributes["picture"] as Map<String, Any>?)
            ?.get("data") as Map<String, Any>?)
            ?.get("url") as String?
}

private class GithubOAuth2UserInfo(attributes: Map<String, Any>) : OAuth2UserInfo(attributes) {
    override val id: String by super.attributes
    override val name: String by super.attributes
    override val email: String by super.attributes
    override val imageUrl: String?
        get() = attributes["avatar_url"] as String?
}

private class GoogleOAuth2UserInfo(attributes: Map<String, Any>) : OAuth2UserInfo(attributes) {
    override val id: String
        get() = attributes["sub"]!! as String
    override val name: String by super.attributes
    override val email: String by super.attributes
    override val imageUrl: String?
        get() = attributes["picture"] as String?
}
