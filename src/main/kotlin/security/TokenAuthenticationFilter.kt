package com.systemkern.springsocialkotlin.security

import com.systemkern.springsocialkotlin.Config
import com.systemkern.springsocialkotlin.ResourceNotFoundException
import com.systemkern.springsocialkotlin.UserRepository
import com.systemkern.springsocialkotlin.findByIdKt
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import java.util.Date
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter

@Component
internal class TokenAuthenticationFilter(
    private var userRepository: UserRepository,
    private val config: Config,
) : OncePerRequestFilter() {

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        addAuthenticationToSecurityContext(request)
        filterChain.doFilter(request, response)
    }

    private val bearerPrefix: String = "Bearer "
    private fun addAuthenticationToSecurityContext(request: HttpServletRequest) {
        try {
            val authHeader = request.getHeader("Authorization")
            val jwtToken = when {
                authHeader.isBlank() -> return
                authHeader.startsWith(bearerPrefix) == false -> return
                else -> authHeader.substring(bearerPrefix.length, authHeader.length)
            }
            if (isTokenValid(authToken = jwtToken, secret = config.tokenSecret) == false) return

            val userId = getUserIdFromToken(jwtToken, config.tokenSecret)
            val userDetails = userRepository.findByIdKt(userId)
                ?.let {
                    UserAuthPrincipal(
                        id = it.id,
                        email = it.email,
                        password = it.password,
                    )
                }
                ?: throw ResourceNotFoundException("User", "id", userId)

            val authentication = UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities)
                .apply { details = WebAuthenticationDetailsSource().buildDetails(request) }
            SecurityContextHolder.getContext().authentication = authentication
        } catch (ex: Exception) {
            logger.error("Could not set user authentication in security context", ex)
        }
    }
}

fun createToken(authentication: Authentication, secret: String, expiration: Long): String =
    Jwts.builder()
        .setSubject("${(authentication.principal as UserAuthPrincipal).id}")
        .setIssuedAt(Date())
        .setExpiration(Date(Date().time + expiration))
        .signWith(SignatureAlgorithm.HS512, secret)
        .compact()

fun isTokenValid(authToken: String?, secret: String): Boolean {
    if (authToken == null) return false
    return try {
        Jwts.parser().setSigningKey(secret).parseClaimsJws(authToken)
        true
    } catch (ex: Throwable) {
        false
    }
}

fun getUserIdFromToken(token: String?, secret: String): Long =
    Jwts.parser()
        .setSigningKey(secret)
        .parseClaimsJws(token)
        .body.subject.toLong()
