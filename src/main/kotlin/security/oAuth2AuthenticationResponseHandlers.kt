package com.systemkern.springsocialkotlin.security

import com.systemkern.springsocialkotlin.Config
import com.systemkern.springsocialkotlin.BadRequestException
import java.net.URI
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler
import org.springframework.stereotype.Component
import org.springframework.web.util.UriComponentsBuilder


@Component
class OAuth2AuthenticationSuccessHandler @Autowired internal constructor(
    private val config: Config,
    private val httpCookieOAuth2AuthorizationRequestRepository: HttpCookieOAuth2AuthorizationRequestRepository,
) : SimpleUrlAuthenticationSuccessHandler() {

    override fun onAuthenticationSuccess(request: HttpServletRequest, response: HttpServletResponse, authentication: Authentication) {
        val targetUrl = determineTargetUrl(request, response, authentication)
        if (response.isCommitted) {
            logger.debug("Response has already been committed. Unable to redirect to $targetUrl")
            return
        }
        clearAuthenticationAttributes(request, response)
        redirectStrategy.sendRedirect(request, response, targetUrl)
    }

    override fun determineTargetUrl(request: HttpServletRequest, response: HttpServletResponse, authentication: Authentication): String {
        val targetUrl: String = request.cookies?.find { it.name == REDIRECT_URI_PARAM_COOKIE_NAME }
            ?.value
            ?.also { uriString ->
                val cookieUri: URI = URI.create(uriString)
                if (config.oAuth2AuthorizedRedirectUris
                        .map { URI.create(it) }
                        .none { it.host.equals(cookieUri.host, ignoreCase = true) && it.port == cookieUri.port }
                // Only validate host and port. Let the clients use different paths if they want to
                ) throw BadRequestException("Sorry! We've got an Unauthorized Redirect URI and can't proceed with the authentication")
            }
            ?: defaultTargetUrl

        return UriComponentsBuilder.fromUriString(targetUrl)
            .queryParam("token", createToken(authentication = authentication, secret = config.tokenSecret, expiration = config.tokenExpirationMsec))
            .build().toUriString()
    }

    protected fun clearAuthenticationAttributes(request: HttpServletRequest, response: HttpServletResponse) {
        super.clearAuthenticationAttributes(request)
        httpCookieOAuth2AuthorizationRequestRepository.removeAuthorizationRequestCookies(request, response)
    }
}


@Component
class OAuth2AuthenticationFailureHandler(
    val httpCookieOAuth2AuthorizationRequestRepository: HttpCookieOAuth2AuthorizationRequestRepository,
) : SimpleUrlAuthenticationFailureHandler() {

    override fun onAuthenticationFailure(request: HttpServletRequest, response: HttpServletResponse, exception: AuthenticationException) {
        var targetUrl = request.cookies?.find { it.name == REDIRECT_URI_PARAM_COOKIE_NAME }
            ?.value
            ?: "/"
        targetUrl = UriComponentsBuilder.fromUriString(targetUrl)
            .queryParam("error", exception.localizedMessage)
            .build().toUriString()
        httpCookieOAuth2AuthorizationRequestRepository.removeAuthorizationRequestCookies(request, response)
        redirectStrategy.sendRedirect(request, response, targetUrl)
    }
}
