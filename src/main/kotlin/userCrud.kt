package com.systemkern.springsocialkotlin

import com.fasterxml.jackson.annotation.JsonIgnore
import com.systemkern.springsocialkotlin.security.CurrentUser
import com.systemkern.springsocialkotlin.security.UserAuthPrincipal
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.Email
import javax.validation.constraints.NotNull
import org.springframework.data.repository.CrudRepository
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController


@RestController
class UserController(
    private val userRepository: UserRepository,
) {
    @GetMapping("/user/me") @PreAuthorize("hasRole('USER')")
    fun getCurrentUser(@CurrentUser userAuthPrincipal: UserAuthPrincipal): User =
        userRepository.findByIdKt(userAuthPrincipal.id)
            ?: throw ResourceNotFoundException("User", "id", userAuthPrincipal.id)
}


@Repository
interface UserRepository : CrudRepository<User, Long> {
    fun findByEmail(email: String): User?
    fun existsByEmail(email: String): Boolean
}

@Suppress("DEPRECATION")
fun <T, ID : Serializable> CrudRepository<T, ID>.findByIdKt(id: ID): T? =
    findById(id).orElse(null)


@Entity
@Table(name = "users")
data class User(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = -1,

    @Column(nullable = false)
    val name: String,

    @Column(nullable = false, unique = true)
    val email: @Email String,

    val imageUrl: String? = null,

    @Column(nullable = false)
    var emailVerified: Boolean = false,

    @JsonIgnore
    val password: String? = null,

    @Enumerated(EnumType.STRING)
    val provider: @NotNull AuthProvider,
    val providerId: String? = null,
)


enum class AuthProvider {
    local, facebook, google, github
}
