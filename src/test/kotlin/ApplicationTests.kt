package com.systemkern.springsocialkotlin

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles

@SpringBootTest(classes = [Config::class])
@ConfigurationPropertiesScan
@ActiveProfiles("test")
class ApplicationTests {

	@Autowired lateinit var config: Config

	@Test
	fun contextLoads() {
		assertThat(config).isNotNull
		assertThat(config.tokenSecret).isEqualTo("test-token-secret")
		assertThat(config.tokenExpirationMsec).isEqualTo(420)
		assertThat(config.oAuth2AuthorizedRedirectUris).contains("http://localhost:3000/oauth2/redirect")
	}
}
