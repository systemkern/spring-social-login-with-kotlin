package com.systemkern.springsocialkotlin

import com.systemkern.springsocialkotlin.security.UserAuthPrincipal
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles

class TokenAuthenticationTests {

	@Test fun `Can create user principal`() {
		val principal = UserAuthPrincipal(
			id = 1,
			email = "email@example.com",
			password = "test-password",
		)
	}
}
