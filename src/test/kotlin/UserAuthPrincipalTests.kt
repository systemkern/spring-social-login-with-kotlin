package com.systemkern.springsocialkotlin;

import com.systemkern.springsocialkotlin.security.UserAuthPrincipal
import com.systemkern.springsocialkotlin.security.createToken
import com.systemkern.springsocialkotlin.security.getUserIdFromToken
import com.systemkern.springsocialkotlin.security.isTokenValid
import io.mockk.every
import io.mockk.mockk
import java.util.UUID.randomUUID
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder


internal class UserAuthPrincipalTests {

    val encryptionSecret = "test-secret"
    val passwordEncoder  = BCryptPasswordEncoder()

    val expectedUserId = 42L
    val authentication: Authentication = mockk {
        every { principal } answers {
            mockk<UserAuthPrincipal> {
                every { id } returns expectedUserId
                every { email } returns "test-email"
                every { password } returns passwordEncoder.encode("test-password")
                every { authorities } returns listOf(SimpleGrantedAuthority("ROLE_USER"))
            }
        }
        every { authorities } returns listOf(SimpleGrantedAuthority("ROLE_USER"))
    }

    @Test fun `Can create and verify token`() {
        val token = createToken(authentication = authentication, secret = encryptionSecret, expiration = 420_000)
        assertThat(isTokenValid(authToken = token, secret = encryptionSecret)).isTrue
        assertThat(getUserIdFromToken(token = token, secret = encryptionSecret)).isEqualTo(expectedUserId)
    }

    @Test fun `Cannot validate expired token`() {
        val token = createToken(authentication = authentication, secret = encryptionSecret, expiration = -999_999)
        assertThat(isTokenValid(authToken = token, secret = encryptionSecret)).isFalse
    }

    @Test fun `Cannot validate invalid token`() {
        assertThat(isTokenValid(authToken = null, secret = encryptionSecret)).isFalse
        assertThat(isTokenValid(authToken = "", secret = encryptionSecret)).isFalse
        assertThat(isTokenValid(authToken = "${randomUUID()}", secret = encryptionSecret)).isFalse
    }
}
